<?php
function add_sidebars(){
	 $args1= array(
        'name'          => __( 'Left Footer Sidebar', 'storefront_child' ),
        'id'            => 'footer-sidebar-1',
        'description'   => __( 'Widgets in this area will be shown on all posts and pages.', 'storefront_child' ),
        'before_widget' => '<div class="col-md-6 left">',
        'after_widget'  => '</div>',
        'before_title'  => '<h2 class="widgettitle">',
        'after_title'   => '</h2>',
    ) ;
	 register_sidebar($args1);
	  $args2= array(
        'name'          => __( 'Right Footer Sidebar', 'storefront_child' ),
        'id'            => 'footer-sidebar-2',
        'description'   => __( 'Widgets in this area will be shown on all posts and pages.', 'storefront_child' ),
        'before_widget' => '<div class="col-md-6 right">',
        'after_widget'  => '</div>',
        'before_title'  => '<h2 class="widgettitle">',
        'after_title'   => '</h2>',
    ) ;
	register_sidebar($args2);
}

add_action('widgets_init', 'add_sidebars');
function wp_storefront_custom_scripts(){
	 wp_enqueue_script( 'custom-cart-script', get_stylesheet_directory_uri() . '/js/custom.js', array(), '1.0.0', true );
}

add_action('wp_enqueue_scripts','wp_storefront_custom_scripts');